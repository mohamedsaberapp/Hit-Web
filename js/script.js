// Quantity functions
function incrementValue() {
    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 0 : value;
    if (value < 10) {
        value++;
        document.getElementById('number').value = value;
    }
}
function decrementValue() {
    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 0 : value;
    if (value > 1) {
        value--;
        document.getElementById('number').value = value;
    }

}
//---------------------//
// Checkout Options 
$(function () {
    //shipping section
    $("#delivery").change(function () {
        $("#pickup").attr("disabled", this.checked);
        $(".shippingOptions .right").toggleClass("halfOpacity", this.checked)
    }).change();

    $("#pickup").change(function () {
        $("#delivery").attr("disabled", this.checked);
        $(".shippingOptions .left").toggleClass("halfOpacity", this.checked)
    }).change();

    //payment section
    $("#credit").change(function () {
        $("#onarrival").attr("disabled", this.checked);
        $(".paymentOptions .right").toggleClass("halfOpacity", this.checked)
    }).change();
    $("#onarrival").change(function () {
        $("#credit").attr("disabled", this.checked);
        $(".paymentOptions .left").toggleClass("halfOpacity", this.checked)
    }).change();
})
//-----------------------//
// Coaches pics
$(function () {
    $('#CoachesPage .images img').mouseover(function () {
        console.log($(this));
        $('#coach-info').addClass('visible', this.mouseover);
        $('#coach-info h2').html('COACH NAME');
    })
    $('#CoachesPage .images img').mouseout(function () {
        console.log($(this));
        $('#coach-info h2').html('SELECT COACH');
    });
    //redirect to coach page
    $('#CoachesPage .images img').mousedown(function () {
        $(window).attr('location', 'single-coach.html');
    })
})


//Program Charts 

var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["JAN", "FEB", "MARCH", "APRIL", "MAY", "JUNE"],
        datasets: [{
            label: 'LOREM',
            data: [12, 19, 3, 5, 2, 3],
            borderColor: '#d71a21',
            backgroundColor: 'transparent',
            radius: '10',
            pointBackgroundColor: '#d71a21',
            pointHoverRadius: '10',
        },
        {
            label: 'IPSUM',
            data: [10, 13, 9, 15, 12, 2],
            borderColor: '#fff',
            backgroundColor: 'transparent',
            pointStyle: 'circle',
            radius: '10',
            pointBackgroundColor: '#fff',
            pointHoverRadius: '10'
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }],
            xAxes: [{
                position: 'top'
            }]
        },
        elements: {
            line: {
                tension: 0, // disables bezier curves
            }
        },
        legend: {
            position: 'bottom',
            labels: {
                padding: 30,
                usePointStyle: true,
            }
        }
    }
});




